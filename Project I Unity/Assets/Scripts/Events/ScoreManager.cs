﻿using TMPro;
using UnityEngine;

namespace Manager
{

    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;
        private int currentScore;

        public static ScoreManager instance;

        private void Awake()
        {
            if (instance == null)
                instance = this;
                
            Debug.Assert(scoreText != null, "scoreText cannot be null");
        }

        public void Init(GameManager gameManager)
        {
            currentScore = 0;
            GameManager.instance.OnRestarted += OnRestarted;
            HideScore(false);
            scoreText.text = $"Score : 0";
        }

        public void SetScore(int score)
        {
            currentScore += score;
            scoreText.text = $"Score : {currentScore}";
        }

        private void OnRestarted()
        {
            GameManager.instance.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
            finalScoreText.text = $"{currentScore}";
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}