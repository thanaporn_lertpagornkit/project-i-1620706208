﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private SoundClip[] soundClips;

    private AudioSource audioSource;

    public static SoundManager instance { get; private set; }

    public void Awake()
    {
        Debug.Assert(soundClips.Length != 0 && soundClips != null, "Sound Clip need to be assigned");
        audioSource = GetComponent<AudioSource>();

        if (instance == null)
            instance = this;

        DontDestroyOnLoad(this);
    }

    public enum Sound
    {
        BGM,
        PlayerFire,
        EnemyFire
    }

    [Serializable]
    public struct SoundClip
    {
        public Sound sound;
        public AudioClip AudioClip;
        [Range(0,1)] public float soundVolume;
    }

    public void Play(AudioSource audioSource, Sound sound)
    {
        var soundClip = GetSoundClip(sound);
        audioSource.clip = soundClip.AudioClip;
        audioSource.volume = soundClip.soundVolume;
        audioSource.Play();
    }

    public void PlayBGM()
    {
        audioSource.loop = true;
        Play(audioSource,Sound.BGM);
    }

    private SoundClip GetSoundClip(Sound sound)
    {
        foreach(var soundClip in soundClips)
        {
            if(soundClip.sound == sound)
            {
                return soundClip;
            }
        }
        return default(SoundClip);
    }
}
