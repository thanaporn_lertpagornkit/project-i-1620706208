﻿using UnityEngine;
using UnityEngine.UI;
using Spaceship;
using System;

namespace Manager
{

    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        [SerializeField] private AudioClip playerExplodedSound;
        [SerializeField] private AudioClip enemyExplodedSound;
        [SerializeField] private float explodedSoundVolume = 2f;

        //[SerializeField] private SoundManager soundManager;


        private int currentEnemySpeed;
        private int enemySpeedIncrease;
        public PlayerSpaceship currentPlayer;
        public EnemySpaceship currentEnemy;


        public static GameManager instance { get; private set; }


        private void Awake()
        {
            if (instance == null)
                instance = this;

            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceshipHp cannot be null");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed cannot be null");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp cannot be null");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed cannot be null");

            currentEnemySpeed = enemySpaceshipMoveSpeed;
            enemySpeedIncrease = 1;

            startButton.onClick.AddListener(OnStartButtonClicked);
            SoundManager.instance.PlayBGM();
            //soundManager.PlayBGM()
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void StartGame()
        {
            ScoreManager.instance.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
            currentPlayer = spaceship;
        }

        private void OnPlayerSpaceshipExploded()
        {
            AudioSource.PlayClipAtPoint(playerExplodedSound, Camera.main.transform.position, explodedSoundVolume);
            Destroy(currentEnemy.gameObject);
            Destroy(currentPlayer.gameObject);
            currentEnemySpeed = enemySpaceshipMoveSpeed;
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemySpaceshipHp, currentEnemySpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
            var playerTransform = currentPlayer.GetComponent<Transform>();
            spaceship.GetComponent<EnemyController>().playerTransform = playerTransform;
            spaceship.GetComponent<EnemyController>().setSpeed(currentEnemySpeed);
            currentEnemy = spaceship;
        }

        private void OnEnemySpaceshipExploded()
        {
            AudioSource.PlayClipAtPoint(enemyExplodedSound, Camera.main.transform.position, explodedSoundVolume);
            ScoreManager.instance.SetScore(1);
            currentEnemySpeed += enemySpeedIncrease;
            SpawnEnemySpaceship();
        }

        private void Restart()
        {
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
            if(currentEnemy != null)
            {
                Debug.Log("destroy enemy");
                Destroy(currentEnemy);
            }
            if(currentPlayer != null)
            {
                Debug.Log("destroy player");
                Destroy(currentPlayer);
            }
        }

        private void Quit()
        {
            Application.Quit();
        }
    }
}
