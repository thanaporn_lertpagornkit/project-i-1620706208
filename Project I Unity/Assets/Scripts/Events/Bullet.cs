﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaceship
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private Rigidbody2D rgdb;

        private string owner;

        //private AudioSource bulletSound;

        public void Init(Vector2 direction, string owner)
        {
            this.owner = owner;
            Move(direction);
            Destroy(gameObject, 1.0f);
        }

        private void Awake()
        {
            Debug.Assert(rgdb != null, "rigidbody2D cannot be null");
            //bulletSound = GetComponent<AudioSource>();
            //bulletSound.Play();
        }

        private void Move(Vector2 direction)
        {
            rgdb.velocity = direction * speed;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.gameObject.CompareTag(owner))
            {
                var target = other.gameObject.GetComponent<IDamagable>();
                target?.TakeHit(damage);
                // ? ไว้เช็คว่า ถ้า target ไม่เป็น null (ไม่มี IDamagable) ก็ให้ทำ TakeHit
                Destroy(gameObject);
            }
            
        }
    }
}
