﻿using UnityEngine;
using System;

namespace Spaceship
{
    public class PlayerSpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;
        [SerializeField] private AudioClip PlayerFireSound;
        [SerializeField] private float playerFireSoundVolume = 0.2f;
        [SerializeField] private Bullet bigBullet;
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(PlayerFireSound != null, "PlayerFireSound cannot be null");
            audioSource = GetComponent<AudioSource>();
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire(Vector2 direction)
        {
            //AudioSource.PlayClipAtPoint(PlayerFireSound, Camera.main.transform.position,playerFireSoundVolume);
            SoundManager.instance.Play(audioSource,SoundManager.Sound.PlayerFire);
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up,"PlayerUnit");
        }

        public void ChargedFire(Vector2 direction)
        {
            SoundManager.instance.Play(audioSource, SoundManager.Sound.PlayerFire);
            var bullet = Instantiate(bigBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up, "PlayerUnit");
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if(Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke(); //เช็คว่ามีใครอยากรู้ว่ายานระเบิดรึเปล่า ก็ Invoke ให้คนนั้นไป
        }
    }
}