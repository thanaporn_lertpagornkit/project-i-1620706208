﻿using Spaceship;
using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;
        private float xMin; //ค่าขอบจอซ้าย
        private float xMax; //ค่าขอบจอขวา
        private float yMin;
        private float yMax;

        private void Awake()
        {
            InitInput();
            CreateMovementBoundary();
        }
        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
            inputActions.Player.ChargeFire.performed += OnChargeFire;
            inputActions.Player.ChargeFire.started += OnChargeFire;
        }

        private void OnChargeFire(InputAction.CallbackContext obj)
        {
            if (obj.started)
            {
                Debug.Log("start");
            }
            else if (obj.performed)
            {
                Debug.Log("performed");
                playerSpaceship.ChargedFire(Vector2.zero);
            }
        }

        private void OnFire(InputAction.CallbackContext obj)
        {
            playerSpaceship.Fire(Vector2.zero);
        }

        public void OnMove(InputAction.CallbackContext obj)
        {
            if (obj.performed)
            {
                movementInput = obj.ReadValue<Vector2>();
            }
            if (obj.canceled)
            {
                movementInput = Vector2.zero;
            }
        }        

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            var inPutVelocity = movementInput * playerSpaceship.Speed;

            var newPosition = transform.position;
            newPosition.x = transform.position.x + inPutVelocity.x * Time.smoothDeltaTime;
            newPosition.y = transform.position.y + inPutVelocity.y * Time.smoothDeltaTime;

            //Clamp movement within boundary
            newPosition.x = Mathf.Clamp(newPosition.x, xMin, xMax);
            newPosition.y = Mathf.Clamp(newPosition.y, yMin, yMax);

            transform.position = newPosition;
        }

        private void CreateMovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null, "mainCamera cannot be null");

            var spriteRenderer = playerSpaceship.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer cannot be null");

            var offset = spriteRenderer.bounds.size;

            //xMin = mainCamera.ViewportToWorldPoint(new Vector2(0, 0)).x + offset.x / 2;
            //xMax = mainCamera.ViewportToWorldPoint(new Vector2(1, 0)).x - offset.x / 2;
            //yMin = mainCamera.ViewportToWorldPoint(new Vector2(0, 0)).y + offset.y / 2;
            //yMax = mainCamera.ViewportToWorldPoint(new Vector2(0, 1)).y - offset.y / 2;

            xMin = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            xMax = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            yMin = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            yMax = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / 2;
        }

        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }
    }
}
