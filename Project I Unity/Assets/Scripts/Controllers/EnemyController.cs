﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spaceship;

public class EnemyController : MonoBehaviour
{
    public Transform playerTransform;
    [SerializeField] private float chasingThresholdDistance;
    private float fireTimer = 0;
    //[SerializeField] private Renderer enemyRenderer;

    public float controlSpeed;

    private EnemySpaceship enemySpaceship;

    private void Awake()
    {
        playerTransform = Manager.GameManager.instance.currentPlayer.GetComponent<Transform>();
        controlSpeed = 1;
        enemySpaceship = GetComponent<EnemySpaceship>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerTransform != null)
        {
            MoveToPlayer();
        }
    }

    private void MoveToPlayer()
    {
        Vector3 enemyPosition = transform.position;
        Vector3 playerPosition = playerTransform.position;

        Vector3 distanceToPlayer = playerPosition - enemyPosition;
        Vector3 moveDirection = distanceToPlayer.normalized * controlSpeed;

        transform.Translate(moveDirection * Time.deltaTime);

        fireTimer += Time.deltaTime;

        if(fireTimer > 1f)
        {
            Vector2 fireDirection = new Vector2(distanceToPlayer.x, distanceToPlayer.y).normalized;
            //Debug.Log(fireDirection);
            enemySpaceship.Fire(fireDirection);
            fireTimer = 0;
        }

    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PlayerUnit"))
        {
            var target = other.gameObject.GetComponent<IDamagable>();
            target?.TakeHit(500);
            // ? ไว้เช็คว่า ถ้า target ไม่เป็น null (ไม่มี IDamagable) ก็ให้ทำ TakeHit
        }

    }

    public void setSpeed(int speed)
    {
        controlSpeed = speed;
    }



    //private void OnDrawGizmos()
    //{
    //    CollosionDebug();
    //}

    //private void CollosionDebug()
    //{
    //    if (enemyRenderer != null && playerRenderer != null)
    //    {
    //        if (intersectAABB(enemyRenderer.bounds, playerRenderer.bounds))
    //        {
    //            Gizmos.color = Color.red;
    //        }
    //        else
    //        {
    //            Gizmos.color = Color.white;
    //        }
    //        Gizmos.DrawWireCube(enemyRenderer.bounds.center, 2 * enemyRenderer.bounds.extents);
    //        Gizmos.DrawWireCube(playerRenderer.bounds.center, 2 * playerRenderer.bounds.extents);
    //    }
    //}


}
